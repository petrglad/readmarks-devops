#!/bin/bash -v
set -e -x

docker run --name ziproxy --detach --restart always -P -p 127.0.0.1:8888:8080 127.0.0.1:5000/wwwproxy

