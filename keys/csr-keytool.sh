#!/bin/bash
set -e

ALIAS=readmarks
KEYSTORE=${ALIAS}.keystore
keytool -keystore $KEYSTORE -alias $ALIAS -genkey -keyalg RSA
keytool -certreq -alias $ALIAS -keystore $KEYSTORE -file ${ALIAS}.csr

# keytool -keystore keystore -import -alias jetty -file jetty.crt -trustcacerts
