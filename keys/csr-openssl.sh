
# https://wiki.eclipse.org/Jetty/Howto/Configure_SSL

openssl genrsa -des3 -out jetty.key
openssl req -new -x509 -key jetty.key -out jetty.crt
openssl req -new -key jetty.key -out jetty.csr
