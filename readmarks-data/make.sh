#!/bin/bash
set -e -x

READMARKS_KEY=$(cat $(dirname $0)"/../.readmarks-key.tmp")
if [ "$READMARKS_KEY" == "" ]
then
  echo "Non empty READMARKS_KEY is required."
  exit 2
fi

cat $(dirname $0)/Dockerfile.template \
 | sed -e "s|@@password@@|$READMARKS_KEY|g" \
 | sudo docker build --no-cache --tag="localhost:5000/readmarks-data" -
echo "$READMARKS_KEY"
