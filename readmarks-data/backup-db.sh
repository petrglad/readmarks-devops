#!/bin/bash
set -e

BACKUP_NAME=readmarks-db-$(date +%Y%m%d-%H%M%S).sql
DATA_CONTAINER=${1:-db}
echo "Container $DATA_CONTAINER" >&2
source .env
docker-compose exec $DATA_CONTAINER /bin/sh \
  -c "env PGPASSWORD=$READMARKS_KEY pg_dump --host=127.0.0.1 --user=readmarks --no-password readmarks" \
  > $BACKUP_NAME

if [ -s $BACKUP_NAME ]
then
  echo $BACKUP_NAME
else
  echo "Backup file is empty." >&2
  rm $BACKUP_NAME
  exit 1
fi
