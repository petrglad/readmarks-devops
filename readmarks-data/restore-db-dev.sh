#!/bin/bash
set -e

DATA_CONTAINER=$1
BACKUP_NAME=$2
sudo docker run --rm -i --link $DATA_CONTAINER:reks-data localhost:5000/readmarks-data /bin/bash \
  -c 'env PGPASSWORD=$REKS_DATA_ENV_READMARKS_DB_PASSWORD psql --host=reks-data --user=readmarks --no-password --set ON_ERROR_STOP=on readmarks'\
  < $BACKUP_NAME > restore.log
echo $BACKUP_NAME
echo restore.log
