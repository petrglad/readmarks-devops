#!/bin/bash
set -e

source .env
docker-compose exec db /bin/sh \
  -c "env PGPASSWORD=$READMARKS_KEY psql --host=127.0.0.1 --user=readmarks --no-password --set ON_ERROR_STOP=on readmarks"
