#!/bin/bash
set -e

BACKUP_NAME=$1

# Using explicit container id with plain docker since "docker-compose exec" stalls on stdin reading.
DATA_CONTAINER=${2:-$(docker-compose ps -q db | head -n 1)}
echo "Restoring into data container  $DATA_CONTAINER" >&2
source .env
docker exec -i $DATA_CONTAINER /bin/sh \
  -c "env PGPASSWORD=$READMARKS_KEY psql --host=127.0.0.1 --user=readmarks --no-password --set ON_ERROR_STOP=on readmarks" \
  < $BACKUP_NAME > restore.log

echo $BACKUP_NAME
echo restore.log
