#!/bin/bash -v
BUILD_DIR="$(pwd)/$(dirname $0)"
set -e -x
READMARKS_JAR="readmarks-0.2.3-SNAPSHOT-standalone.jar"
KEYSTORE_FILE="readmarks.keystore"
JAR_BUILD_DIR="$BUILD_DIR/build/readmarks"
cd "$JAR_BUILD_DIR"
git pull --ff-only
lein clean
lein uberjar
cd "$BUILD_DIR"
cp "$JAR_BUILD_DIR/target/$READMARKS_JAR" $READMARKS_JAR
cp "$HOME/Documents/work/readmarks/$KEYSTORE_FILE" $KEYSTORE_FILE

sudo docker build --tag="localhost:5000/readmarks" .

rm "$READMARKS_JAR" "$KEYSTORE_FILE"

