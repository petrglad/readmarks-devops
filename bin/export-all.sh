#!/bin/bash
NAME="docker-images-$(date +%Y%m%d-%H%M).tar"
echo "Exporting to $NAME"

docker save petrglad/postgresql >> $NAME
docker save petrglad/java7 >> $NAME
# docker save petrglad/readmarks >>$NAME

bzip2 "$NAME"
