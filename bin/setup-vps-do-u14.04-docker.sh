#!/bin/bash


setup_local_docker_registry() {
    # https://www.digitalocean.com/community/tutorials/how-to-set-up-a-private-docker-registry-on-ubuntu-14-04
    apt-get -y install build-essential python-dev libevent-dev python-pip liblzma-dev
    pip install docker-registry
    echo "Create docker registry default config (cp config_sample.yml config.yml) in "
    gunicorn --access-logfile - --debug -k gevent -b 0.0.0.0:5000 -w 1 docker_registry.wsgi:application | grep FileNotFoundError
    echo 
    mkdir -p /var/log/docker-registry
    DOCKER_REGISTRY_CONF=/etc/service/docker-registry
    mkdir -p $DOCKER_REGISTRY_CONF
    cat $DOCKER_REGISTRY_CONF/run <<EOF
#!/bin/sh
gunicorn --access-logfile /var/log/docker-registry/access.log --error-logfile /var/log/docker-registry/server.log -k gevent --max-requests 100 --graceful-timeout 3600 -t 3600 -b localhost:5000 -w 8 docker_registry.wsgi:application
EOF
    chmod +x $DOCKER_REGISTRY_CONF/run
}


setup_wwwproxy_service() {
    docker pull petrglad/wwwproxy
    docker run --detach=true --publish=8888:8080 --name ziproxy petrglad/wwwproxy
    # docker stop ziproxy # not really necessary
    WWWPROXY_CONF=/etc/service/wwwproxy
    mkdir -p $WWWPROXY_CONF
    cat >$WWWPROXY_CONF/run <<EOF
#!/bin/sh
docker --restart=false start --attach ziproxy
EOF
    chmod +x $WWWPROXY_CONF/run
}


# Note: Docker does not resolve DNS via IPV6: disable IPv6 in /etc/resolv.conf
echo 'TODO (digitalocean-specific): DOCKER_OPTS="--dns 8.8.8.8 --dns 8.8.4.4" in /etc/default/docker'

apt-get install -y htop daemontools-run

# now start supervise of daemontools (what is the correct way to do this?)
