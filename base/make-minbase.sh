#!/bin/bash
set -e -x

# sudo apt-get update 
# sudo apt-get install debian-keyring gnupg
# sudo apt-get install docker.io debootstrap
# sudo /usr/share/docker-engine/contrib/mkimage.sh \
sudo /usr/share/docker.io/contrib/mkimage.sh \
  -t localhost:5000/minbase debootstrap \
  --variant=minbase \
  --keep-debootstrap-dir \
  stable http://mirror.yandex.ru/debian/

