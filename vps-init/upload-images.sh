#!/bin/bash -v
set -e -x

PORT=5000
REPO=localhost:$PORT
VPS=readmarks.net 

sudo docker start registry
sudo docker push $REPO/readmarks-data
sudo docker push $REPO/readmarks
sudo docker push $REPO/nginx
sudo docker push $REPO/wwwproxy
