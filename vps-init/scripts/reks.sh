#!/bin/bash
set -e -x

export CRYPT_DIR="/root/certs"
for i in $CRYPT_DIR/readmarks.net.{key,crt};
do
  if ! [ -f $i ]
  then
    echo "Key file is required: $i"
    exit 1
  fi
done

export CHALLENGE_DIR="/root/acme-challenge"
mkdir -p $CHALLENGE_DIR

DH_PARAMS="$CRYPT_DIR/dhparams.pem"
if ! [ -f $DH_PARAMS ]
then
  echo "Generating new dhparams"
  openssl dhparam -out $DH_PARAMS 2048
fi

docker-compose $@
