#!/bin/bash
set -e -x

./reks.sh pull

BACKUP_FILE=$(./backup-db.sh)

echo "Backup file is"
ls -l "$BACKUP_FILE"

read -p "Proceed with upgrade? (yes/no)" CONFIRM
if [ "yes" != "$CONFIRM" ]
then 
  echo "Cancelled"
  exit 2
fi

./reks.sh stop
./reks.sh rm --force
./reks.sh up -d db
./restore-db.sh "$BACKUP_FILE"
./reks.sh up -d
