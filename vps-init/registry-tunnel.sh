#!/bin/bash -v
set -e -x

PORT=5000
REPO=localhost:$PORT
VPS=readmarks.net

# Run registry locally, listen in VPS
TUNNEL_CMD="ssh -f -N -T -R $PORT:127.0.0.1:5000 root@$VPS"
echo "Tunnel cmd is $TUNNEL_CMD"


echo "Killing existing tunnel(s)..."
ps ax | grep "$TUNNEL_CMD" | awk '{print $1;}' | xargs kill || true

echo "Openning repository tunnel..."
$TUNNEL_CMD
echo "Done"
