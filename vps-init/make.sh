#!/bin/bash 
set -e -x

HERE=$(dirname $0)
OUT=readmarks-scripts.tar.gz

SCRIPTS=scripts
TARGET="$HERE/target"
SCRIPTS_TARGET="$TARGET/$SCRIPTS"

rm -fr "$SCRIPTS_TARGET"
mkdir --parents "$SCRIPTS_TARGET"

READMARKS_KEY=$(cat "$HERE/../.readmarks-key.tmp")
if [ "$READMARKS_KEY" == "" ]
then
  echo "Non empty READMARKS_KEY is required."
  exit 2
fi
echo "READMARKS_KEY=$READMARKS_KEY" > "$SCRIPTS_TARGET/.env"

cp $HERE/scripts/* "$SCRIPTS_TARGET"
cp $HERE/../readmarks-data/*-db.sh "$SCRIPTS_TARGET"

cd "$TARGET" # Strip build dir from archive paths
tar czf "readmarks-scripts.tar.gz" "$SCRIPTS"

echo "$TARGET/$OUT"
