#!/bin/bash -v
set -e -x

REPO="localhost:5000"

base/make-minbase.sh
sudo docker build --no-cache=true --tag="$REPO/base" base
sudo docker build --tag="$REPO/java8" java8
sudo docker build --tag="$REPO/postgresql" postgresql

openssl rand -base64 30 > .readmarks-key.tmp
readmarks-data/make.sh
readmarks/make.sh
sudo docker build --tag="$REPO/nginx" nginx
vps-init/make.sh

# Extras

sudo docker build --tag="$REPO/wwwproxy" wwwproxy
# sudo docker build --tag="$REPO/lighttpd" lighttpd

